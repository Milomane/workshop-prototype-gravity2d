﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedSoundObject : MonoBehaviour
{
    void Start()
    {
        Destroy(gameObject, GetComponent<AudioSource>().clip.length);
    }
}
