﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public GameObject uiEndLevel;

    public GameObject coin1;
    public GameObject coin2;
    public GameObject coin3;

    public string nextLevel;

    public AudioSource audioSource;
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>())
        {
            audioSource.Play();
            
            // Level end
            uiEndLevel.SetActive(true);
            
            other.GetComponent<PlayerController>().StopTimer();
            
            switch (other.GetComponent<PlayerController>().coinCount)
            {
                case 0 :
                    break;
                case 1 :
                    coin1.SetActive(true);
                    break;
                case 2 :
                    coin1.SetActive(true);
                    coin2.SetActive(true);
                    break;
                case  3 :
                    coin1.SetActive(true);
                    coin2.SetActive(true);
                    coin3.SetActive(true);
                    break;
            }

            StartCoroutine(WaitChangeLevel());
        }
    }

    public IEnumerator WaitChangeLevel()
    {
        yield return new WaitForSeconds(2f);
        
        SceneManager.LoadScene(nextLevel);
    }
}
