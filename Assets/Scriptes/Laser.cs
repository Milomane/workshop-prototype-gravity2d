﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Transform laserPos;

    public GameObject particleObject;

    public LayerMask layer;

    public bool laserOn;
    
    public AudioSource audioSource;
    public AudioSource constantLaserNoise;
    public AudioClip laserDeathClip;
    
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    
    void Update()
    {
        if (laserOn)
        {
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, laserPos.position - Vector3.forward);

            RaycastHit2D hit;
            if (hit = Physics2D.Raycast(laserPos.position, transform.up, layer))
            {
                lineRenderer.SetPosition(1, new Vector3(0, 0, -.1f) + (Vector3)hit.point);

                particleObject.SetActive(true);
                
                particleObject.transform.position = (Vector3)hit.point - new Vector3(0,0,2);
                particleObject.transform.rotation = Quaternion.FromToRotation(particleObject.transform.up, hit.normal) * particleObject.transform.rotation;
                
                if (hit.collider.GetComponent<PlayerController>())
                {
                    if (!audioSource.isPlaying)
                    {
                        if (!hit.collider.GetComponent<PlayerController>().dead)
                        {
                            audioSource.clip = laserDeathClip;
                            audioSource.Play();
                        }
                    }
                    
                    hit.collider.GetComponent<PlayerController>().Death();
                }

            }
            else
            {
                particleObject.SetActive(false);
                lineRenderer.SetPosition(1, new Vector3(0, 0, -.1f) + transform.up * 100);
            }

            /*if (!constantLaserNoise.isPlaying)
            {
                constantLaserNoise.Play();
            }*/
            
        }
        else
        {
            constantLaserNoise.Stop();
            audioSource.Stop();
            particleObject.SetActive(false);
            lineRenderer.enabled = true;
        }
        
    }
}
