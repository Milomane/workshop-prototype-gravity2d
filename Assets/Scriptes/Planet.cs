﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    public float gravityMultiplier;
    public bool reverseGravity;
    public bool planetTurning;
    public float turningSpeed = 45f;

    void Start()
    {
        
    }

    void Update()
    {
        if (planetTurning)
        {
            transform.Rotate(Vector3.forward, turningSpeed * Time.deltaTime);
        }
    }
}
