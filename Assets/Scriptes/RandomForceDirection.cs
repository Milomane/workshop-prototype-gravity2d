﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomForceDirection : MonoBehaviour
{
    public float reboundDistance = 8;
    private Rigidbody2D rb;

    public float randomForceApplied;
    public float rotationForce;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        
        rb.AddForce(new Vector2((float)Random.Range(-randomForceApplied, randomForceApplied), (float)Random.Range(-randomForceApplied, randomForceApplied)), ForceMode2D.Impulse);
        rb.AddTorque((float)Random.Range(-rotationForce, rotationForce), ForceMode2D.Impulse);
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, Vector3.zero) > reboundDistance)
        {
            rb.AddForce((Vector2.zero + new Vector2(Random.Range(-10, 10), Random.Range(-10, 10)) - (Vector2)transform.position).normalized, ForceMode2D.Impulse);
            rb.AddTorque((float)Random.Range(-rotationForce, rotationForce), ForceMode2D.Impulse);
        }
    }
}
