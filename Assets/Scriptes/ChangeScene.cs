﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public string levelToLoad = "Level1";
    

    public void LoadLevel()
    {
        SceneManager.LoadScene(levelToLoad);
    }
}
