﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveOnStart : MonoBehaviour
{
    public GameObject activeThis;

    // Start is called before the first frame update
    void Start()
    {
        activeThis.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
