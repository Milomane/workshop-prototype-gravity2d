﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject soundObject;
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>())
        {
            Instantiate(soundObject, transform.position, Quaternion.identity);
            other.GetComponent<PlayerController>().coinCount++;
            Destroy(gameObject);
        }
    }
}
