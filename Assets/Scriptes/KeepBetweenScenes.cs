﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeepBetweenScenes : MonoBehaviour
{
    public static bool spawned;
    public bool survive;
    public string stopAtLevel = "Level7";
    
    void Start()
    {
        if (!survive)
        {
            if (!spawned)
            {
                spawned = true;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        else
        {
            spawned = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    
    void Update()
    {
        if (SceneManager.GetActiveScene().name == stopAtLevel)
        {
            spawned = false;
            Destroy(gameObject);
        }
    }
}
