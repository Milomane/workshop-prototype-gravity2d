﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    public float jumpForce;
    public float speed;
    
    public int maxJump = 2;
    int jump;
    public GameObject flame;    
    public GameObject flame2;
    public bool flameOn1;
    private int count;
    public int toCount1;
    public bool flameOn2;
    public int toCount2;
    public float friction = .25f;

    public Transform feet;
    public Transform spriteTransform;

    public int coinCount;

    public TextMeshProUGUI textTimer;
    private bool timerRun = true;
    private float timer = 0;

    public Animator animator;
    public GameObject fadeOut;
    public bool dead;

    public AudioSource audioSource;
    public AudioClip deathClip;
    public AudioClip jumpClip;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jump = maxJump;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerRun)
            timer += Time.deltaTime;
        
        textTimer.text = Mathf.FloorToInt(timer / 60) + "m : " + Mathf.FloorToInt(timer - Mathf.FloorToInt(timer / 60) * 60) + "s";
        
        RaycastHit2D hit;
        if (hit = Physics2D.Raycast(feet.position, transform.right * .01f, .01f))
        {
            rb.velocity -= rb.velocity * friction;
            
            jump = maxJump;

            if (!dead)
            {
                if (Input.GetKey(KeyCode.Q))
                {
                    rb.AddForce(-transform.up * speed);
                    spriteTransform.localScale = new Vector3(-.6f, .6f, .6f);
                }

                if (Input.GetKey(KeyCode.D))
                {
                    rb.AddForce(transform.up * speed);
                    spriteTransform.localScale = new Vector3(.6f, .6f, .6f);
                }
            }
        }


        if (!dead)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (jump > 0)
                {
                    if (!flameOn1)
                    {
                        flame.SetActive(true);
                        flameOn1 = true;
                    }
                    jump--;
                    audioSource.clip = jumpClip;
                    audioSource.Play();
                    rb.AddForce(-transform.right * jumpForce, ForceMode2D.Impulse);
                }
            }

            if (flameOn1)
            {
                count++;
                if (count > toCount1)
                {
                    flame.SetActive(false);
                    flameOn1 = false;
                    flame2.SetActive(true);
                    flameOn2 = true;
                }
            }

            if (flameOn2)
            {
                count++;
                if (count > toCount2)
                {
                    flame2.SetActive(false);
                    flameOn2 = false;
                    count = 0;
                }
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                Death();
            }
        }
    }

    public void Death()
    {
        if (!dead)
        {
            dead = true;
            audioSource.clip = deathClip;
            audioSource.Play();
            StartCoroutine(DeathEffects());
        }
    }

    public IEnumerator DeathEffects()
    {
        fadeOut.SetActive(true);
        animator.SetBool("Dead", true);
        yield return new WaitForSeconds(1.8f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StopTimer()
    {
        timerRun = false;
        textTimer.color = Color.green;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(feet.position, transform.right*.01f);
    }
}
